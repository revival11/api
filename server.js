var express = require('express')
var bodyParser = require('body-parser')
var app = express ()
app.use(bodyParser.json())

var requestJson = require('request-json')

var apiKey = "apiKey=Tg4panDMLPBK2o9jSb2YI5so_VMsPnQ7"
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancovp/collections"
//var clienteMlab

var port = process.env.PORT || 3000
var fs = require ('fs')

app.listen(port)

console.log("API escuchando en el puerto " + port)

var err

console.log("Hasta el infinito, y más allá!")

/*
Comienzo a definir los recursos que pongo disponibles
*/

app.get ('/apitechu/v5/usuarios', function(req, res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    //console.log(clienteMlab)
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
          res.send(body)
        }
    })
})

app.get ('/apitechu/v5/usuarios/:id', function(req, res) {
    var id = req.params.id
    var query = 'q={"id":' + id + '}'
    //var filtro = 'l=2&f={"_id":0}'
    var filtro
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + filtro + "&" + apiKey)
    //console.log(clienteMlab)
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
          if (body.length > 0) {
            //res.send({"first_name":body[0].first_name, "last_name":body[0].last_name})
            res.send(body)
          }
          else {
            res.status(404).send("Usuario no encontrado")
          }
          //res.send(body)
        }
    }) // Fin clienteMlab get
})


app.get ('/apitechu/v5/prueba', function(req, res) {
    var client
    client = require('request-json').createClient('https://my.api.mockaroo.com/')
    console.log("\n"+JSON.stringify(client))
    client.get('iban?key=6514abf0', function(err, resP1, bodyP1) {
    //client.get('iban', {name: 'test'}, {headers: {'Content-size': 34}}, function(err, res, body) {
    res.send(bodyP1)
  });
})


// Recurso para dar de alta usuarios con una IBAN generado por Mockaroo
// Recurso para dar de alta usuarios con una IBAN generado por Mockaroo

app.post ('/apitechu/v5/usuarios/add', function(req, res) {
    var apiKeyMockaroo = "key=6514abf0"
    var urlMockaroo = "https://my.api.mockaroo.com"
    var clienteMockaroo
    var datos = req.body
    var idcliente
    // La query que me he creado para meter movimientos fake https://my.api.mockaroo.com/movimientos?min_id=10&&max_id=10&key=6514abf0

    // Buscamos todos los clientes y los contamos para conocer el ultimo
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get('', function(err, httpResponse, body) {
          idcliente = body.length + 1 // Sumamos uno al id del ultimo movimiento y lo guardamos para pasarselo a Mockaroo luego
          datos['id'] = idcliente // Aqui modificamos el id que nos ha llegado (o no porque no hace falta) con el que hemos calculado nosotros.

          // Añado el nuevo usuario en MLAB
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
          clienteMlab.post('', datos, function(err, httpResponse, body) {
              if (err) {
                  console.log("HTTP_Code: " + httpResponse.statusCode + "\nError: "+err)
                  res.status(httpResponse.statusCode).send("Error, la has liado!!")
              }
              else {
                //console.log("\n0-ADDUSER. Resultado OK. Usuario añadido: "+JSON.stringify(datos.id))
                console.log("\n0-ADDUSER. Resultado OK. Usuario añadido: "+datos.id+" "+body.first_name+" "+body.last_name+" "+body.email+" "+body['_id'].$oid)

              } //Fin if err clienteMlab
          }) //Fin clienteMlab post


          // Usando el API de Mockaroo y el schema que creamos, le enviamos el idcliente para que nos genere un IBAN y unos movimientos ficticios
          clienteMockaroo = requestJson.createClient(urlMockaroo + "/movimientos?min_id=" + idcliente + "&max_id=" + idcliente + "&" + apiKeyMockaroo)
          clienteMockaroo.get('', function(err, httpResponse, bodyMockaroo) {
              if (err) {
                  console.log("\nERROR MOCKAROO"+err)
                  res.status(404).send("Error peticionando a la API de Mockaroo")
              }
              else {
                  //Ahora añado los movimientos que he creado con Mockaroo a MLAB
                  clienteMlab2 = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
                  clienteMlab2.post('', bodyMockaroo, function(err, httpResponse, body) {
                    console.log("\n6-ADDUSER POST con los nuevos movimientos Mockaroo: "+JSON.stringify(bodyMockaroo))
                    if (err) {
                        console.log("HTTP_Code: " + httpResponse.statusCode + "\nError: "+err)
                        res.status(httpResponse.statusCode).send("Error, la has liado!!")
                    }
                    else {
                      console.log("\n7-ADDUSER Respuesta de MLAB al update: "+JSON.stringify(body))
                      res.send({"Resultado":"OK", "Movimientos":body})
                    }

                  }) //Fin clienteMlab2 put

              } //Fin err clienteMockaroo
          }) // Fin clienteMockaroo get

      }) //Fin clienteMlab get

}) //Fin recurso



// Recurso para dar de baja usuarios
// Recurso para dar de baja usuarios

app.post ('/apitechu/v5/usuarios/del', function(req, res) {
  var email = req.headers.email
  var query = 'q={"email":"' + email + '"}'
  var filtro = 'l=1'//&f={"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + filtro + "&" + apiKey)
  console.log("\n"+JSON.stringify(clienteMlab.host))
  clienteMlab.get('', function(err, resM, body) {
      if (!err) {
            if (body.length > 0) {
              //res.send({"first_name":body[0].first_name, "last_name":body[0].last_name, "_id":body[0]._id})
              console.log("\n"+JSON.stringify(body[0].first_name)+" "+JSON.stringify(body[0]._id.$oid))
              clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios/"+ body[0]._id.$oid + "?" + apiKey)
              console.log("\n"+JSON.stringify(clienteMlab.host))
              clienteMlab.del('', function (errP, resP, bodyP) { // No funciona metiendole modificaciones en el primer parametro como en el ejemplo de la linea de abajo
              //clienteMlab.del('/usuarios/'+ body[0]._id.$oid + '?' + apiKey, function (errP, resP, bodyP) {
                      console.log("\n"+JSON.stringify(clienteMlab))
                      if (!errP) {
                        //res.send({"borrado":"ok", "first_name":body[0].first_name, "last_name":body[0].last_name})
                        console.log("HTTP_Code: " + resP.statusCode + "\nError: "+errP)
                        res.send(bodyP)
                      }
                      else {
                        //res.send(body)
                        res.status(404).send("Error al borrar el usuario")
                      } // End if errP
              }) //End clienteMlab del

            }
            else {
                res.status(404).send("Usuario erroneo")
            } // End if body.length > 0
      } //end if!err

    }) //Fin clienteMlab get
}) //Fin recurso


// Recurso para hacer login
// Recurso para hacer login

app.post ('/apitechu/v5/usuarios/login', function(req, res) {
    var email = req.headers.email
    var password = req.headers.password
    var id = req.params.id
    var query = 'q={"email":"' + email + '","password":"' + password + '"}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
    console.log("\n"+JSON.stringify(clienteMlab.host))
    clienteMlab.get('', function(err, resM, body) {
        if (!err) {
          if (body.length == 1) {
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
            var cambio = '{"$set":{"logged":true}}'
            clienteMlab.put('?q={"id": '+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
              console.log("\n"+JSON.stringify(clienteMlab.host))
              if (!errP) {
                res.send({"login":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name})
              }
              else {
                res.status(404).send("Error al actualizar el estado del usuario")
              }

            })

          }
          else {
            res.status(404).send("Usuario o Password erroneo")
          }
          //res.send(body)
        }
    }) //Fin clienteMlab
}) //Fin recurso


// Recurso para hacer logout
// Recurso para hacer logout

app.post('/apitechu/v5/usuarios/logout', function(req,res){
  var id = req.headers.id
  var query = 'q={"id":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) // estaba logado
      {
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put('?q={"id":'+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP)
        {
          res.send({"logout":"ok", "id":body[0].id})
        })
      }
      else {
        res.status(200).send('Usuario no logado previamente')
      }
    }
  })
})


// Recurso para recuperar las cuentas IBAN de clientes
// Recurso para recuperar las cuentas IBAN de clientes

app.post ('/apitechu/v5/cuentas', function(req, res) {
    var idcliente = req.headers.idcliente
    var query = 'q={"idcliente":' + idcliente + '}'
    var filter = 'f={"IBAN":1,"_id":0}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
    console.log(clienteMlab.host)
    clienteMlab.get('', function(err, resM, body) {
        console.log(body)
        if(!err) {
          res.send(body)
        }

      })
  })



// Recurso para hacer recuperar saldo de una cuenta por su IBAN
// Recurso para hacer recuperar saldo de una cuenta por su IBAN

app.post ('/apitechu/v5/saldo', function(req, res) {
  var idcuenta = req.headers.idcuenta
  var query = 'q={"IBAN":' + idcuenta + '}'
  var filter = 'f={"saldo":1, "movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
  console.log("\n"+JSON.stringify(clienteMlab.host))
  clienteMlab.get('', function(err, resM, body) {
  if (!err) {
      if (body.length > 0) // Cliente encontrado
      {
          console.log("\n"+body[0].saldo)
          res.send({"saldo":body[0].saldo})
      }
      else {
        res.status(404).send("Cuenta no encontrada")
      }
    }
  }) // Fin clienteMlab get
})


// Recurso para hacer recuperar movimientos de una cuenta por su IBAN
// Recurso para hacer recuperar movimientos de una cuenta por su IBAN

  app.post ('/apitechu/v5/movimientos', function(req, res) {
    var idcuenta = req.headers.idcuenta
    var query = 'q={"IBAN":' + idcuenta + '}'
    var filter = 'f={"movimientos":1,"_id":0}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
    console.log("\n"+JSON.stringify(clienteMlab.host))
    clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length > 0) // Cliente encontrado
        {
          res.send(body[0].movimientos)
        }
        else {
          res.status(404).send("Cuenta no encontrada")
        }
      }
    }) // Fin clienteMlab get
  })



  // // Recurso para hacer añadir movimientos a una cuenta por su IBAN
  // // Recurso para hacer añadir movimientos a una cuenta por su IBAN

    app.post ('/apitechu/v5/movimientos/add', function(req, res) {
      var datos = req.body
      var idcuenta = req.headers.idcuenta
      var query = 'q={"IBAN":' + idcuenta + '}'
      clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
      console.log("\n"+JSON.stringify(clienteMlab.host))
      clienteMlab.get('', function(err, httpResponse, body) {
        if (err) {
            res.status(404).send("Cuenta no encontrada")
        }
        else {
            console.log("\nADD Movimiento 01-Datos del movimiento a añadir: "+JSON.stringify(datos))
            console.log("\nADD Movimiento 02-Total Movimientos: "+body[0].movimientos.length)
            console.log("\nADD Movimiento 03-Array movimientos: "+JSON.stringify(body))
            datos.id = (body[0].movimientos.length) + 1 // Sumamos uno al id del ultimo movimiento
            console.log("\nADD Movimiento 04-Datos del movimiento a añadir con el ID incorporado: "+JSON.stringify(datos))
            //body[0].movimientos.push(datos) // Cualquiera de los dos metodos vale para acceder al valor del array
            body[0]['movimientos'].push(datos) // Cualquiera de los dos metodos vale para acceder al valor del array. Vaya mierda de sistema
            console.log("\nADD Movimiento 05-Array movimientos modificado: "+JSON.stringify(body))

        } // Fin clienteMlab if

        var filter = 'f={"movimientos":1,"_id":1}&l=1'
        //clienteMlab2 = requestJson.createClient(urlMlabRaiz + "/cuentas/"+ body[0]._id.$oid +"?" + apiKey)
        clienteMlab2 = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
        clienteMlab2.put('', body, function(err, httpResponse, body) {
          console.log("\nADD Movimiento 06-Peticion PUT final para update: "+JSON.stringify(clienteMlab2.host))
          if (err) {
              console.log("HTTP_Code: " + res.statusCode + "\nError: "+err)
              res.status(res.statusCode).send("Error, la has liado!!")
          }
          else {
            console.log("\nADD Movimiento 07-Respuesta de MLAB al update: "+JSON.stringify(body))
            res.send({"Resultado":"OK", "IBAN":idcuenta, "Movimiento":datos})
          }

        }) //Fin clienteMlab2 put
      })//Fin clienteMlab get
})
