var mocha = require ('mocha')
var chai = require ('chai')
var chaiHttp = require ('chai-http')
var server = require ('../server')

var should = chai.should()

chai.use(chaiHttp) // Configurar chai con modo http

describe('Test de conectividad', () => {
  it('Google funciona', (done) =>{
    chai.request('http://www.google.es')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})


describe('Test de API de usuarios', () => {
  it('Funciona el API', (done) =>{
    chai.request('http://localhost:3000')
        .get('/apitechu/v5/usuarios')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          //res.body.mensaje.should.be.eql("Bienvenido a mi mundo!")
            res.body.should.be.a('array')
          done()
        })
  })
  it('Lista de Usuarios', (done) =>{
    chai.request('http://localhost:3000')
        .get('/apitechu/v5/usuarios')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('first_name')
            res.body[i].should.have.property('last_name')
            res.body[i].should.have.property('email')
            res.body[i].should.have.property('password')
          }
          done()
        })
  })
})
